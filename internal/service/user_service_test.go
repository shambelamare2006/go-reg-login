package service_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/shambel-amare/go-reg-login/internal/service"
	"github.com/shambel-amare/go-reg-login/mocks"
	"github.com/stretchr/testify/assert"

	db "github.com/shambel-amare/go-reg-login/database"
)

func TestRegister(t *testing.T) {
	testCases := []struct {
		Name               string
		MockregisteredUser db.User
		MockFetchedUser    db.User
		Request            db.CreateUserParams
		MockRegisterError  error
		MockFetchError     error
	}{
		{
			Name: "New User registeration",

			MockregisteredUser: db.User{
				FirstName: "jhon",
				LastName:  "doe",
				Email:     "jhon@gmail.com",
				Password:  "secret",
			},
			Request: db.CreateUserParams{
				FirstName: "jhon",
				LastName:  "doe",
				Email:     "jhon@gmail.com",
				Password:  "secret",
			},
			MockRegisterError: nil,
			MockFetchedUser: db.User{
				FirstName: "jhon",
				LastName:  "doe",
				Email:     "jhon@gmail.com",
				Password:  "secret",
			},
			MockFetchError: errors.New("User Already exist"),
		},
		{
			Name: "Existing User registeration",
			MockregisteredUser: db.User{
				FirstName: "user10",
				LastName:  "doe",
				Email:     "user10@gmail.com",
				Password:  "secret",
			},
			Request: db.CreateUserParams{
				FirstName: "user10",
				LastName:  "doe",
				Email:     "user10@gmail.com",
				Password:  "secret",
			},
			MockRegisterError: errors.New("User Already exist"),
			MockFetchedUser:   db.User{},
			MockFetchError:    nil,
		},
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockUserRepository := mocks.NewMockUserRepository(ctrl)

	userService := service.NewUserService(&service.USConfig{
		UserRepository: mockUserRepository,
	})
	// mockUserService := mocks.NewMockUserService(ctrl)
	for _, test := range testCases {
		t.Run(test.Name, func(t *testing.T) {

			mockUserRepository.EXPECT().GetUser(gomock.Any(), test.Request.Email).Return(test.MockFetchedUser, test.MockFetchError).MinTimes(1)
			if test.MockFetchError == nil {
				mockUserRepository.EXPECT().CreateUser(gomock.Any(), gomock.Any()).Return(test.MockregisteredUser, test.MockRegisterError).MinTimes(1)
			}
			user, err := userService.Signup(context.Background(), &test.Request)

			if err != nil {
				assert.Error(t, err)
				// assert.Equal(t, expectedResponse.Err, err)
			}
			if err == nil {
				assert.Equal(t, test.MockregisteredUser.Email, user.Email)
			}
		})
	}
}
