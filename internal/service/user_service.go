package service

import (
	"context"
	"log"

	_ "github.com/golang/mock/mockgen/model"
	"github.com/joomcode/errorx"

	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/shambel-amare/go-reg-login/internal/apperrors"
	"github.com/shambel-amare/go-reg-login/internal/repository"

	"github.com/shambel-amare/go-reg-login/util"
)

//go:generate mockgen -destination=../../mocks/mock_UserService.go -package=mocks github.com/shambel-amare/go-reg-login/internal/service UserService

type UserService interface {
	Signup(ctx context.Context, u *db.CreateUserParams) (db.User, error)
	Signin(ctx context.Context, email, password string) (db.User, error)
}

type USConfig struct {
	UserRepository  repository.UserRepository
	TokenRepository repository.TokenRepository
	TokenMaker      JWTMaker
}

func NewUserService(r *USConfig) UserService {
	return &USConfig{
		UserRepository: r.UserRepository,
	}
}

// Signup reaches our to a UserRepository to verify if the
// email address is available and signs up the user if this is the case
func (s *USConfig) Signup(ctx context.Context, u *db.CreateUserParams) (db.User, error) {
	fetcheduser, err := s.UserRepository.GetUser(ctx, u.Email)
	if err == nil && len(fetcheduser.Email) != 0 {
		//wrap the error with errorx?
		return db.User{}, apperrors.DuplicateUser.Wrap(err, "user already exist")
	}
	hashedPassword, err := util.HashPassword(u.Password)
	u.Password = hashedPassword
	if err != nil {
		log.Printf("error hashing password:%v", err)
		return db.User{}, errorx.InternalError.Wrap(err, "error hashing password")
	}

	createdUser, err := s.UserRepository.CreateUser(ctx, *u)
	if err != nil {
		return db.User{}, err
	}
	return createdUser, nil
}

// Signin reaches out to a UserRepository check if the user exists
// and then compares the supplied password with the provided password
// if a valid email/password combo is provided, u will hold all
// available user fields
func (s *USConfig) Signin(ctx context.Context, email, password string) (db.User, error) {
	uFetched, err := s.UserRepository.GetUser(ctx, email)

	// Will return NotAuthorized to client to omit details of why
	if err != nil {
		//wrap the error with errorx?
		return db.User{}, apperrors.IllegalArgument.Wrap(err, "Invalid Email or Password")
	}

	matchErr := util.CheckPassword(uFetched.Password, password)

	if matchErr != nil {
		return db.User{}, apperrors.IllegalArgument.Wrap(matchErr, "Invalid email and password combination")
	}

	return uFetched, nil
}
