package response

import "github.com/gin-gonic/gin"

type Response struct {
	Data   interface{} `json:"data,omitempty"`
	Error  interface{} `json:"error,omitempty"`
	Status int         `json:"status"`
}

func SuccessResponse(c gin.Context, r Response) {
	c.JSON(r.Status, r)
}
