package handler

// // used to help extract validation errors
// type invalidArgument struct {
// 	Field string `json:"field"`
// 	Value string `json:"value"`
// 	Tag   string `json:"tag"`
// 	Param string `json:"param"`
// }

// func bindincommingData(ctx *gin.Context, req interface{}) bool {

// 	if err := ctx.ShouldBind(req); err != nil {
// 		log.Printf("Error binding data:%+v %v\n", err, req)
// 		if errs, ok := err.(validator.ValidationErrors); ok {
// 			var invalidArgs []invalidArgument

// 			for _, err := range errs {
// 				invalidArgs = append(invalidArgs, invalidArgument{
// 					err.Field(),
// 					err.Value().(string),
// 					err.Tag(),
// 					err.Param(),
// 				})
// 			}

// 			err := apperrors.NewBadRequest("Invalid request parameters. See invalidArgs")

// 			ctx.JSON(err.Status(), gin.H{
// 				"error":       err,
// 				"invalidArgs": invalidArgs,
// 			})
// 			return false
// 		}
// 		// if we aren't able to properly extract validation errors,
// 		// we'll fallback and return an internal server error
// 		fallBack := apperrors.NewInternal()

// 		ctx.JSON(fallBack.Status(), gin.H{"error": fallBack})
// 		return false
// 	}
// 	return true
// }
