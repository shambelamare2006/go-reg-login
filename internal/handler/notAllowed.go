package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (h *Handler) NotAllowed(ctx *gin.Context) {

	ctx.JSON(http.StatusOK, gin.H{
		"Message": "Authorized",
	})
}
