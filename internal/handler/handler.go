package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/go-reg-login/internal/repository"
	"github.com/shambel-amare/go-reg-login/internal/service"
)

type Handler struct {
	UserService     service.UserService
	TokenService    service.TokenService
	TokenRepository repository.TokenRepository
}
type UserHandler interface {
	SignUp(ctx *gin.Context)
	SignIn(ctx *gin.Context)
	NotAllowed(ctx *gin.Context)
}
type Config struct {
	UserService     service.UserService
	TokenService    service.TokenService
	TokenRepository repository.TokenRepository
}

func Initialize(c *Config) UserHandler {
	handler := &Handler{
		UserService:     c.UserService,
		TokenService:    c.TokenService,
		TokenRepository: c.TokenRepository,
	}
	return handler
}
