package handler

import (
	"regexp"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
)

func (req signupReq) Validate() error {
	return validation.ValidateStruct(&req,
		// Street cannot be empty, and the length must between 5 and 50
		validation.Field(&req.FirstName, validation.Required.Error("First Name is required"), validation.Length(3, 50)),
		// City cannot be empty, and the length must between 5 and 50
		validation.Field(&req.LastName, validation.Required.Error("Last Name is Required"), validation.Length(3, 50).Error("Last name should be more than 3 letters")),
		// State cannot be empty, and must be req string consisting of two letters in upper case
		validation.Field(&req.Password, validation.Required.Error("Password is required"), validation.Match(regexp.MustCompile("^[A-Za-z0-9]{6}")).Error("Password should be Minimum six characters, at least one letter and one number")),
		// State cannot be empty, and must be req string consisting of five digits
		validation.Field(&req.Email, validation.Required.Error("Email is required"), is.Email.Error("Email Should be correct email format")),
		validation.Field(&req.Phone, validation.Required.Error("Phone number is required"), validation.Match(regexp.MustCompile("^[0-9]{10}")).Error("Phone number must be ten digits long")),
	)

}
