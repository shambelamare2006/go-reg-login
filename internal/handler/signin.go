package handler

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/shambel-amare/go-reg-login/internal/apperrors"
)

type userResponse struct {
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Email     string    `json:"email"`
	CreatedAt time.Time `json:"created_at"`
}

type loginUserRequest struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required,min=6"`
}

type loginUserResponse struct {
	SessionID             uuid.UUID    `json:"session_id"`
	AccessToken           string       `json:"access_token"`
	AccessTokenExpiresAt  time.Time    `json:"access_token_expires_at"`
	RefreshToken          string       `json:"refresh_token"`
	RefreshTokenExpiresAt time.Time    `json:"refresh_token_expires_at"`
	User                  userResponse `json:"user"`
}

func newUserResponse(user db.User) userResponse {
	return userResponse{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		CreatedAt: user.Createdat,
	}
}
func (h *Handler) SignIn(ctx *gin.Context) {
	var req loginUserRequest
	if err := ctx.ShouldBindJSON(&req); err != nil {
		err = apperrors.IllegalArgument.Wrap(err, "binding faild")
		ctx.Error(err)
		return
	}

	user, err := h.UserService.Signin(ctx, req.Email, req.Password)
	if err != nil {
		ctx.Error(err)
		return
	}

	accessToken, accessPayload, err := h.TokenService.CreateToken(user.Email, 60*time.Minute)
	if err != nil {
		ctx.Error(err)
		return
	}

	refreshToken, refreshPayload, err := h.TokenService.CreateToken(
		user.Email,
		60*time.Minute,
	)
	if err != nil {
		ctx.Error(err)
		return
	}

	session, err := h.TokenRepository.CreateTokenSession(ctx, db.CreateSessionParams{
		ID:           refreshPayload.ID,
		Email:        user.Email,
		RefreshToken: refreshToken,
		UserAgent:    ctx.Request.UserAgent(),
		ClientIp:     ctx.ClientIP(),
		IsBlocked:    false,
		ExpiresAt:    refreshPayload.ExpiredAt,
	})
	if err != nil {
		ctx.Error(err)
		return
	}

	rsp := loginUserResponse{
		SessionID:             session.ID,
		AccessToken:           accessToken,
		AccessTokenExpiresAt:  accessPayload.ExpiredAt,
		RefreshToken:          refreshToken,
		RefreshTokenExpiresAt: refreshPayload.ExpiredAt,
		User:                  newUserResponse(user),
	}
	ctx.JSON(http.StatusOK, SuccessResponse(rsp))
}

func SuccessResponse(resp loginUserResponse) gin.H {
	return gin.H{
		"user_data": resp,
		"message":   "successful",
	}
}
