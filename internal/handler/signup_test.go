package handler_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	_ "github.com/lib/pq"

	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/shambel-amare/go-reg-login/internal/handler"
	"github.com/shambel-amare/go-reg-login/internal/routes"
	"github.com/shambel-amare/go-reg-login/mocks"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
)

func TestSignupHandler(t *testing.T) {
	suite.Run(t, new(SignupHandlerTestSuite))
}

type SignupHandlerTestSuite struct {
	suite.Suite
	signupService *mocks.MockUserService
	signupRepo    mocks.MockUserRepository
	underTest     handler.UserHandler
}

// setup the test state
func (suite *SignupHandlerTestSuite) SetupTest() {
	mockCtrl := gomock.NewController(suite.T())
	defer mockCtrl.Finish()
	suite.signupService = mocks.NewMockUserService(mockCtrl)
	suite.signupRepo = *mocks.NewMockUserRepository(mockCtrl)
	h := handler.Initialize(&handler.Config{
		UserService: suite.signupService,
	})
	suite.underTest = h
}

type Response struct {
	User    db.User
	Message string
}

func (suite *SignupHandlerTestSuite) TestSignUp() {
	//setup gin in testmode
	gin.SetMode(gin.TestMode)

	uRes := db.User{
		FirstName: "jhon",
		LastName:  "doe",
		Email:     "jhon@gmail.com",
	}
	suite.signupService.EXPECT().Signup(gomock.Any(), gomock.AssignableToTypeOf(&db.CreateUserParams{})).Return(uRes, nil).Times(1)
	suite.signupRepo.EXPECT().CreateUser(gomock.Any(), gomock.AssignableToTypeOf(&db.CreateUserParams{})).Return(uRes, nil).Times(1)
	suite.signupRepo.EXPECT().GetUser(gomock.Any(), gomock.AssignableToTypeOf(db.CreateUserParams{}.Email)).Return(uRes, nil).Times(0)

	rr := httptest.NewRecorder()
	router := gin.Default()
	routes.SetUpRouter(router, suite.underTest)
	reqBody, _ := json.Marshal(gin.H{
		"first_name": "jhon",
		"last_name":  "doe",
		"email":      "jhon@gmail.com",
		"password":   "secretsecret",
	})
	// use bytes.NewBuffer to create a reader
	request, err := http.NewRequest(http.MethodPost, "/api/signup", bytes.NewBuffer(reqBody))
	if err != nil {
		err := fmt.Errorf("error on request: %v", err)
		log.Fatal(err)
	}

	request.Header.Set("Content-Type", "application/json")
	router.ServeHTTP(rr, request)
	//assertions
	response := rr.Result()
	suite.Equal("201 Created", response.Status)

	defer response.Body.Close()
	res := Response{}
	json.NewDecoder(response.Body).Decode(&res)
	suite.Equal("jhon", res.User.FirstName)
}
func TestSignup(t *testing.T) {
	//setup gin in testmode
	gin.SetMode(gin.TestMode)

	t.Run("All inuts are required", func(t *testing.T) {

		MockregisteredUser := db.User{
			FirstName: "jhon",
			LastName:  "doe",
			Email:     "jhongmailcom",
			Password:  "secret",
		}
		CalledErr := errors.New("Caller called")
		ExpectErr := false
		ctrl := gomock.NewController(t)

		defer ctrl.Finish()
		mockUserService := mocks.NewMockUserService(ctrl)
		mockUserService.EXPECT().Signup(context.Background(), &MockregisteredUser).Return(MockregisteredUser, CalledErr).Times(0)
		// a response recorder for getting written http response
		rr := httptest.NewRecorder()

		router := gin.Default()
		h := handler.Initialize(&handler.Config{
			UserService: mockUserService,
		})
		routes.SetUpRouter(router, h)
		// create a request body with empty email and password

		reqBody, _ := json.Marshal(MockregisteredUser)
		// use bytes.NewBuffer to create a reader
		request, err := http.NewRequest(http.MethodPost, "/api/signup", bytes.NewBuffer(reqBody))
		if ExpectErr {
			assert.Error(t, err, "signup error:", err)
		} else {
			assert.NoError(t, err, "signup error:", err)
		}
		assert.NoError(t, err)

		request.Header.Set("Content-Type", "application/json")

		router.ServeHTTP(rr, request)

		assert.Equal(t, 400, rr.Code)

	})

	t.Run("Invalid email", func(t *testing.T) {
		MockregisteredUser := db.User{
			FirstName: "jhon",
			LastName:  "doe",
			Email:     "jhongmail.com",
			Password:  "secret1414",
		}
		MockRegisterError := errors.New("Field Validation Eamil")
		ExpectErr := false
		// We just want this to show that it's not called in this case
		ctrl := gomock.NewController(t)

		defer ctrl.Finish()
		mockUserService := mocks.NewMockUserService(ctrl)

		mockUserService.EXPECT().Signup(context.Background(), MockregisteredUser).Return(MockregisteredUser, MockRegisterError).Times(0)

		// a response recorder for getting written http response
		rr := httptest.NewRecorder()

		// don't need a middleware as we don't yet have authorized user
		router := gin.Default()

		h := handler.Initialize(&handler.Config{
			UserService: mockUserService,
		})
		routes.SetUpRouter(router, h)
		// create a request body with empty email and password
		reqBody, err := json.Marshal(gin.H{
			"first_Name": "jhon",
			"last_Name":  "doe",
			"email":      "jhongmail.com",
			"password":   "secret1414",
		})
		assert.NoError(t, err)

		// use bytes.NewBuffer to create a reader
		request, err := http.NewRequest(http.MethodPost, "/api/signup", bytes.NewBuffer(reqBody))

		if ExpectErr {
			assert.Error(t, err, "signup error:", err)
		} else {
			assert.NoError(t, err, "signup error:", err)
		}
		assert.NoError(t, err)

		request.Header.Set("Content-Type", "application/json")

		router.ServeHTTP(rr, request)

		assert.Equal(t, 400, rr.Code)
	})
}
