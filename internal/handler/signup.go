package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/shambel-amare/go-reg-login/internal/apperrors"
)

//signup request paramater struct used for validation, marshalling and unmarshalling
type signupReq struct {
	FirstName string `json:"first_name" `
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Phone     int64  `json:"phone"`
	Password  string `json:"password"`
}

//signup handler
func (h *Handler) SignUp(c *gin.Context) {
	var req signupReq

	//bind incoming json to struct and check for validation error
	json.NewDecoder(c.Request.Body).Decode(&req)
	err := req.Validate()
	if err != nil {
		err = apperrors.ValidationFaild.Wrap(err, "one or more invalid inputs")
		c.Error(err)
		return
	}
	// transform req to database struct
	u := db.CreateUserParams(req)
	createdUser, err := h.UserService.Signup(c, &u)
	if err != nil {
		c.Error(err)
		return
	}

	c.JSON(http.StatusCreated, Response{
		User: response(createdUser),
	})

}

type response struct {
	ID        uuid.UUID `json:"id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Password  string    `json:"hashed_password"`
	Email     string    `json:"email"`
	Createdat time.Time `json:"createdat"`
}
type Response struct {
	User    response `json:"user,omitempty"`
	Message string   `json:"message"`
}
