package middlewares

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shambel-amare/go-reg-login/internal/service"
)

//AuthorizeJWT -> to authorize JWT Token
func AuthorizeJWT() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		secretKey := "sajlsjdfflajfowfjlajoiuojelaijofwelrjlaldfjoieflaldfiohaljsdflqefljaljdfiefha"
		const BearerSchema string = "Bearer "
		authHeader := ctx.GetHeader("Authorization")
		if authHeader == "" {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": "No Authorization header found"})

		}
		tokenString := authHeader[len(BearerSchema):]
		TokenService, err := service.NewJWTMaker(secretKey)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": "Can not initiate Token service"})
		}
		if token, err := TokenService.VerifyToken(tokenString); err != nil {

			fmt.Println("token", tokenString, err.Error())
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error": "Not Valid Token"})

		} else {

			if err := token.Valid(); err == nil {
				ctx.Set("userID", token.ID)
				ctx.Next()
			} else {
				ctx.AbortWithStatus(http.StatusUnauthorized)
			}

		}

	}

}
