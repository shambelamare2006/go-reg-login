package routes

import (
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"github.com/shambel-amare/go-reg-login/internal/apperrors"
	"github.com/shambel-amare/go-reg-login/internal/handler"
	"github.com/shambel-amare/go-reg-login/internal/middlewares"
)

func SetUpRouter(router *gin.Engine, h handler.UserHandler) {
	g := router.Group("/api")

	if gin.Mode() != gin.TestMode {
		g.Use(middlewares.Timeout(5*time.Second, apperrors.NewServiceUnavailable()))
	}
	g.Use(middlewares.ErrorHandler())

	g.POST("/signup", h.SignUp)
	g.POST("/signin", h.SignIn)
	g.POST("/details", middlewares.AuthorizeJWT(), h.NotAllowed)
}
