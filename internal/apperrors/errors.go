package apperrors

import (
	"errors"
	"fmt"
	"net/http"
)

// Kind holds a Kind string and integer code for the error
type Kind string

// "Set" of valid errorKinds
const (
	Authorization      Kind = "AUTHORIZATION"       // Authentication Failures -
	BadRequest         Kind = "BADREQUEST"          // Validation errors / BadInput
	Conflict           Kind = "CONFLICT"            // Already exists (eg, create account with existent email) - 409
	Internal           Kind = "INTERNAL"            // Server (500) and fallback errors
	NotFound           Kind = "NOTFOUND"            // For not finding resource
	ServiceUnavailable Kind = "SERVICE_UNAVAILABLE" // For long running handlers
)

// Different types of error returned by the VerifyToken function
var (
	ErrInvalidToken = errors.New("token is invalid")
	ErrExpiredToken = errors.New("token has expired")
)

// Error holds a custom error for the application
// which is helpful in returning a consistent
// error Kind/message from API endpoints
type Error struct {
	Kind    Kind   `json:"kind"`
	Message string `json:"message"`
}

// Error satisfies standard error interface
// we can return errors from this package as
// a regular old go _error_
func (e *Error) Error() string {
	return e.Message
}

// Status is a mapping errors to status codes
// Of course, this is somewhat redundant since
// our errors already map http status codes
func (e *Error) Status() int {
	switch e.Kind {
	case Authorization:
		return http.StatusUnauthorized
	case BadRequest:
		return http.StatusBadRequest
	case Conflict:
		return http.StatusConflict
	case Internal:
		return http.StatusInternalServerError
	case NotFound:
		return http.StatusNotFound
	default:
		return http.StatusInternalServerError
	}
}

// Status checks the runtime Kind
// of the error and returns an http
// status code if the error is model.Error
func Status(err error) int {
	var e *Error
	if errors.As(err, &e) {
		return e.Status()
	}
	return http.StatusInternalServerError
}

/*
* Error "Factories"
 */

// NewAuthorization to create a 401
func NewAuthorization(reason string) *Error {
	return &Error{
		Kind:    Authorization,
		Message: reason,
	}
}

// NewBadRequest to create 400 errors (validation, for example)
func NewBadRequest(reason string) *Error {
	return &Error{
		Kind:    BadRequest,
		Message: fmt.Sprintf("Bad request. Reason: %v", reason),
	}
}

// NewConflict to create an error for 409
func NewConflict(name string, value string) *Error {
	return &Error{
		Kind:    Conflict,
		Message: fmt.Sprintf("resource: %v with value: %v already exists", name, value),
	}
}

// NewInternal for 500 errors and unknown errors
func NewInternal() *Error {
	return &Error{
		Kind:    Internal,
		Message: fmt.Sprintln("Internal server error."),
	}
}

// NewNotFound to create an error for 404
func NewNotFound(name string, value string) *Error {
	return &Error{
		Kind:    NotFound,
		Message: fmt.Sprintf("resource: %v with value: %v not found", name, value),
	}
}

// NewServiceUnavailable to create an error for 503
func NewServiceUnavailable() *Error {
	return &Error{
		Kind:    ServiceUnavailable,
		Message: "Service unavailable or timed out",
	}
}
