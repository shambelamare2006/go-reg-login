package repository

import (
	"context"

	_ "github.com/golang/mock/mockgen/model"

	"github.com/google/uuid"
	db "github.com/shambel-amare/go-reg-login/database"
)

//go:generate mockgen -destination=../../mocks/mock_TokenRepository.go -package=mocks github.com/shambel-amare/go-reg-login/internal/repository TokenRepository

type TokenRepository interface {
	CreateTokenSession(ctx context.Context, params db.CreateSessionParams) (db.Session, error)

	GetTokenSession(ctx context.Context, id uuid.UUID) (db.Session, error)
}

func NewTokenRepository(db *db.Queries) TokenRepository {
	return &pgRepository{
		DB: db,
	}
}
func (r *pgRepository) CreateTokenSession(ctx context.Context, params db.CreateSessionParams) (db.Session, error) {
	return r.DB.CreateSession(ctx, params)
}
func (r *pgRepository) GetTokenSession(ctx context.Context, id uuid.UUID) (db.Session, error) {
	return r.DB.GetSession(ctx, id)
}
