package repository

import (
	"context"

	_ "github.com/golang/mock/mockgen/model"

	db "github.com/shambel-amare/go-reg-login/database"
)

//go:generate mockgen -destination=../../mocks/mock_UserRepository.go -package=mocks github.com/shambel-amare/go-reg-login/internal/repository UserRepository

type UserRepository interface {
	GetUser(ctx context.Context, email string) (db.User, error)
	CreateUser(ctx context.Context, arg db.CreateUserParams) (db.User, error)
}

type pgRepository struct {
	DB *db.Queries
}

func NewUserRepository(db *db.Queries) UserRepository {
	return &pgRepository{
		DB: db,
	}
}

func (r *pgRepository) GetUser(ctx context.Context, email string) (db.User, error) {
	return r.DB.GetUser(ctx, email)
}
func (r *pgRepository) CreateUser(ctx context.Context, user db.CreateUserParams) (db.User, error) {
	return r.DB.CreateUser(ctx, user)
}
