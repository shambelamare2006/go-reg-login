FROM golang:alpine as builder

WORKDIR /go/src/app

ENV GO111MODULE=on

RUN go mod download

COPY . .

RUN go build -o ./run

FROM alpine:latest

RUN apk --no-cashe add ca-certificates
WORKDIR /root/
#copy the built excutible from the builder stage
COPY --from=builder /go/src/app/run .

EXPOSE 8080
CMD ["./run"]