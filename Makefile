DB_URL=postgresql://twof:twof@localhost:26257/userdb?sslmode=disable
roach:
	cockroach sql --url 'postgresql://root@localhost:26257/defaultdb?sslmode=disable'
createdb:
	docker exec -it go-reg-login_roach1_1 createdb --username=root --owner=root userdb
dropdb:
	docker exec -it postgres dropdb userdb
migrateup:
	docker run -v /home/twof/Desktop/go-tutorial/go-reg-login/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://twof:twof@localhost:26257/userdb?sslmode=disable" up
migrateup2:
	docker run -v /home/twof/Desktop/go-tutorial/go-reg-login/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://twof:twof@localhost:26257/userdb?sslmode=disable" up 2
migratedown:
	docker run -v /home/twof/Desktop/go-tutorial/go-reg-login/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://twof:twof@localhost:26257/userdb?sslmode=disable" down 1

migratedownall:
	docker run -v /home/twof/Desktop/go-tutorial/go-reg-login/database/migrations:/migrations --network host migrate/migrate -path=/migrations/ -database "cockroachdb://twof:twof@localhost:26257/userdb?sslmode=disable" down -all
sqlc:
	sqlc generate
.PHONY: createdb dropdb migrateup migrateup2 migratedown migratedownall sqlc roach