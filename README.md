# Go Hexagonal architecture
* /main:
        - the main package sets up handlers, services and repositories 
        - set up the server to listen on a specific port and
        - forward them to trafic to handlers
* /internal/handler: 
        these are the adapters of the left side in the #hexagonal architecture
            - this handler package has an http addapter that depends on user services which are
            the implementations of the core ports
            -the core ports are the interfaces defined in the core package which defines the expected behaviour that a service has to fulfill inorder to satisfy thier interface
* /internal/database: 
        this includes migrations and query sql codes for the sqlc library to      
        generate our domain structs and postgress database service implementation of the business logic we planned to excute.
            -migrations are also used by the migrate/migrate library to migrate up or down oour database
* /internal/core: 
        contains the auto generated codes by sqlc with some test and additional
            interfaces which are our ports
