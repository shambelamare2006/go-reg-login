package db_test

import (
	"context"
	"testing"

	_ "github.com/lib/pq"
	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/stretchr/testify/require"
)

func TestUserRegister(t *testing.T) {
	arg := db.CreateUserParams{
		FirstName: "jhon",
		LastName:  "doe",
		Email:     "jhon@gmail.com",
		Password:  "secret",
	}
	user, err := testQueries.CreateUser(context.Background(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, user)
	require.Equal(t, arg.FirstName, user.FirstName)
	require.Equal(t, arg.LastName, user.LastName)
	require.Equal(t, arg.Email, user.Email)
	require.NotZero(t, user.ID)
	require.NotZero(t, user.Createdat)

}
