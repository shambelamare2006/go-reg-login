package db_test

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"testing"

	db "github.com/shambel-amare/go-reg-login/database"

	_ "github.com/lib/pq"
)

const (
	DBdriver = "postgres"
	host     = "localhost"
	port     = 26257
	username = "twof"
	password = "twof"
	dbname   = "userdb"
)

var testQueries *db.Queries

func TestMain(m *testing.M) {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, username, password, dbname)
	conn, err := sql.Open(DBdriver, psqlInfo)
	if err != nil {
		log.Fatal("cannot connect to db:", err)
	}

	testQueries = db.New(conn)

	os.Exit(m.Run())
}
