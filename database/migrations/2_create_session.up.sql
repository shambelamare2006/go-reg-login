CREATE TABLE IF NOT EXISTS sessions (
  id uuid PRIMARY KEY,
  email VARCHAR NOT NULL,
  refresh_token VARCHAR NOT NULL,
  user_agent VARCHAR NOT NULL,
  client_ip INET NOT NULL,
  is_blocked BOOLEAN NOT NULL DEFAULT false,
  expires_at TIMESTAMPTZ NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT (now())
);

ALTER TABLE sessions ADD FOREIGN KEY (email) REFERENCES users (email);