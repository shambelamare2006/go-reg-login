Feature: user registration to the system
        In order to log in to the system
        As a user 
        I can register to the system so that 
        I can log in to the system and user services.
@new
Scenario Outline: registeration of new user
    Given I am not a registered user with email "<Email>" 
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password  |
    |<Email>|<FirstName>|<LastName>|<Password>|
    Then I should get confirmation of success "<ConfirmationOfSuccess>" with status code 201
    Examples:
        |            Email|  FirstName|  LastName|  Password|               ConfirmationOfSuccess|
        |   jhon@gmail.com|  jhon|       doe|        secret|    User succesfuly registered|
        |   jane@gmail.com|  jane|       doe|        secret|    User succesfuly registered|
        

@existing
Scenario Outline: registration of already registered user 
    Given I am a registered user with email "<Email>" 
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password | 
    |<Email>|<FirstName>|<LastName>|<Password>|
    Then I should get an error with status code 409 
    Examples:
        |   Email|  FirstName|  LastName|   Password|              
        |   jhon@gmail.com|  jhon|  doe|   secret|     
      
@invalid
Scenario Outline: registration with invalid or incomplete form 
    Given I am not a registered user with email "<Email>" 
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password | 
    |<Email>|<FirstName>|<LastName>|<Password>|
    Then I should get an error with status code 400
    Examples:
        |   Email|  FirstName|  LastName|   Password|              
        |    jhongmail.com|  jhon|       doe|        secret|          
        |       jhon@gmail|  jhon|       doe|        secret|          
        |  jhon@gmail.com|  jhon|       doe|            ""|         
        |  jhon@gmail.com|  jhon|        ""|        secret|          
        |  jhon@gmail.com|    ""|       doe|        secret|         
        |               ""|  jhon|       doe|        secret|        
   