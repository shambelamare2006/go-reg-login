package features

import (
	"bytes"
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/shambel-amare/go-reg-login/internal/handler"
	"github.com/shambel-amare/go-reg-login/internal/repository"
	"github.com/shambel-amare/go-reg-login/internal/routes"
	"github.com/shambel-amare/go-reg-login/internal/service"
	"github.com/shambel-amare/go-reg-login/util"

	"github.com/cucumber/godog"
	_ "github.com/lib/pq"
)

var rr = httptest.NewRecorder()

const (
	DBdriver = "postgres"
	host     = "localhost"
	port     = 26257
	username = "twof"
	password = "twof"
	dbname   = "userdb"
)

type RequestForm struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Password  string `json:"password"`
}

type ExpectedResponse struct {
	User    db.User `json:"user"`
	Message string  `json:"message"`
}

var list = make([]RequestForm, 0)
var cockroachInfo string = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, username, password, dbname)

func dbConnection(database string) *db.Queries {

	connection, err := sql.Open(DBdriver, database)
	if err != nil {
		log.Printf("error connection to db: %s", err)
		return nil
	}
	conn := db.New(connection)
	return conn
}
func DeleteUser(c context.Context, email string) error {
	log.Printf("Deleting----:%v", email)
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()
	err := pconn.DeleteUser(c, email)
	return err
}
func DeleteAllUser(c context.Context) error {
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()
	err := pconn.DeleteAllUser(c)
	return err
}
func RegisterUser(c context.Context, user db.User) error {
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()
	Password, err := util.HashPassword(user.Password)
	if err != nil {
		log.Printf("error hashing password:%v", err)
		return err
	}
	u := db.CreateUserParams{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Password:  Password,
	}
	_, dbErr := pconn.CreateUser(c, u)
	return dbErr
}
func NewService() (service.UserService, service.TokenService, repository.TokenRepository) {
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()

	NewUserRepository := repository.NewUserRepository(pconn)

	userService := service.NewUserService(&service.USConfig{
		UserRepository: NewUserRepository,
	})
	secretKey := "alkfhalkfhalkfhalkfhalkfhalkfhalkfh"
	tokenService, _ := service.NewJWTMaker(secretKey)
	tokenRepository := repository.NewTokenRepository(pconn)
	return userService, tokenService, tokenRepository
}
func iAmARegisteredUser(email string) error {
	users := []db.User{
		{
			FirstName: "jhon",
			LastName:  "doe",
			Email:     email,
			Password:  "secret",
		},
	}
	var ErrReg error
	for _, user := range users {
		ErrReg = RegisterUser(context.Background(), user)
	}
	return ErrReg
}
func iAmNotARegisteredUserWithEmail(email string) error {
	err := DeleteUser(context.Background(), email)
	return err
}
func iShouldGetAnErrorWithStatusCode(status int) error {
	errresponse := new(ExpectedResponse)

	if rr.Code != status {
		err := errors.New("response code do not match")
		log.Printf("Want [%d], got [%d]\n", http.StatusBadRequest, rr.Code)
		return err
	}
	// decoder := json.NewDecoder(rr.Body)
	// if err := decoder.Decode(&errresponse); err != nil {
	// 	log.Printf("error decoding recorder body to ExpectedResponse")
	// 	return err
	// }
	json.Unmarshal(rr.Body.Bytes(), &errresponse)

	log.Printf("DECODED:%v\n", errresponse)

	// if errresponse.Message != arg {
	// 	err := errors.New("error message do not match")
	// 	log.Printf("Want [%s], got [%s]\n", arg, errresponse.Message)
	// 	return err
	// }
	return nil
}

func iShouldGetConfirmationOfSuccessWithStatusCode(message string, status int) error {
	Response := new(ExpectedResponse)

	if rr.Code != status {
		err := errors.New("response code do not match")
		log.Printf("Want [%d], got [%d]\n", http.StatusCreated, rr.Code)
		return err
	}
	//: Use Decoding, it is faster than unmarshaling
	decoder := json.NewDecoder(rr.Body)
	if err := decoder.Decode(&Response); err != nil {
		log.Printf("error decoding recorder body to ExpectedResponse")
		return err
	}
	log.Printf("DECODED:%v\n", Response)
	//unmarshaling is 25% slower than Decoding
	// json.Unmarshal(rr.Body.Bytes(), &ExpectedResponse)

	// if Response.Message != arg {
	// 	err := errors.New("confirmation do not match")
	// 	log.Printf("Want [%s], got [%s]\n", arg, Response.Message)
	// 	return err
	// }
	return nil
}

func iSubmitTheRegistrationFormWithTheFollowingDetails(userinfo *godog.Table) error {
	//setup gin in testmode
	gin.SetMode(gin.TestMode)
	//set httptest response recrder
	router := gin.Default()
	userService, tokenService, tokenRepository := NewService()
	h := handler.Initialize(&handler.Config{
		UserService:     userService,
		TokenService:    tokenService,
		TokenRepository: tokenRepository,
	})
	routes.SetUpRouter(router, h)
	for i := 1; i < len(userinfo.Rows); i++ {
		list = append(list, RequestForm{
			Email:     userinfo.Rows[i].Cells[0].Value,
			FirstName: userinfo.Rows[i].Cells[1].Value,
			LastName:  userinfo.Rows[i].Cells[2].Value,
			Password:  userinfo.Rows[i].Cells[3].Value,
		})

	}
	// create a request body
	reqBody, err := json.Marshal(gin.H{
		"first_name": list[0].FirstName,
		"last_name":  list[0].LastName,
		"email":      list[0].Email,
		"password":   list[0].Password,
	})
	if err != nil {
		log.Printf("Marshaling error")
		return err
	}

	// use bytes.NewBuffer to create a reader
	request, err := http.NewRequest(http.MethodPost, "/api/signup", bytes.NewBuffer(reqBody))
	if err != nil {
		log.Printf("Request sending error")
		return err
	}
	request.Header.Set("Content-Type", "application/json")

	router.ServeHTTP(rr, request)

	return nil
}

// func TestFeatures(t *testing.T) {
// 	suite := godog.TestSuite{
// 		ScenarioInitializer: InitializeScenario,
// 		Options: &godog.Options{
// 			Format:   "pretty",
// 			Paths:    []string{"features"},
// 			TestingT: t, // Testing instance that will run subtests.
// 		},
// 	}

// 	if suite.Run() != 0 {
// 		t.Fatal("non-zero status returned, failed to run feature tests")
// 	}
// }

func InitializeScenario(Sc *godog.ScenarioContext) {
	Sc.Before(func(ctx context.Context, sc *godog.Scenario) (context.Context, error) {
		// email := sc.Steps[1].Argument.DataTable.Rows[1].Cells[0].Value
		// err := DeleteUser(ctx, email)
		list = make([]RequestForm, 0)
		rr = httptest.NewRecorder()
		return ctx, nil
	})
	Sc.After(func(ctx context.Context, sc *godog.Scenario, err error) (context.Context, error) {
		email := sc.Steps[1].Argument.DataTable.Rows[1].Cells[0].Value
		Err := DeleteUser(ctx, email)
		return ctx, Err
	})

	Sc.Step(`^I am a registered user with email "([^"]*)"$`, iAmARegisteredUser)
	Sc.Step(`^I am not a registered user with email "([^"]*)"$`, iAmNotARegisteredUserWithEmail)
	Sc.Step(`^I am not a registered user with email "([^"]*)"""$`, iAmNotARegisteredUserWithEmail)
	Sc.Step(`^I submit the registration form with the following details$`, iSubmitTheRegistrationFormWithTheFollowingDetails)
	Sc.Step(`^^I should get an error with status code (\d+)$`, iShouldGetAnErrorWithStatusCode)
	Sc.Step(`^I should get confirmation of success "([^"]*)" with status code (\d+)$`, iShouldGetConfirmationOfSuccessWithStatusCode)
}
