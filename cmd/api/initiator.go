package main

import (
	"database/sql"
	"fmt"
	"log"

	db "github.com/shambel-amare/go-reg-login/database"
	"github.com/shambel-amare/go-reg-login/internal/handler"
	"github.com/shambel-amare/go-reg-login/internal/repository"
	"github.com/shambel-amare/go-reg-login/internal/service"
)

func initializeHandler() handler.UserHandler {

	userService, tokenService, tokenRepository := Inject()

	h := handler.Initialize(&handler.Config{
		UserService:     userService,
		TokenService:    tokenService,
		TokenRepository: tokenRepository,
	})
	return h
}

const (
	DBdriver = "postgres"
	host     = "localhost"
	port     = 26257
	username = "twof"
	password = "twof"
	dbname   = "userdb"
)

var cockroachInfo string = fmt.Sprintf("host=%s port=%d user=%s "+
	"password=%s dbname=%s sslmode=disable",
	host, port, username, password, dbname)

func dbConnection(database string) *db.Queries {

	connection, err := sql.Open(DBdriver, database)
	if err != nil {
		log.Printf("error connection to db: %s", err)
		return nil
	}
	conn := db.New(connection)
	return conn
}
func Inject() (service.UserService, service.TokenService, repository.TokenRepository) {
	pconn := dbConnection(cockroachInfo)
	defer pconn.Close()

	NewUserRepository := repository.NewUserRepository(pconn)

	userService := service.NewUserService(&service.USConfig{
		UserRepository: NewUserRepository,
	})
	secretKey := "alkfhalkfhalkfhalkfhalkfhalkfhalkfh"
	tokenService, _ := service.NewJWTMaker(secretKey)
	tokenRepository := repository.NewTokenRepository(pconn)
	return userService, tokenService, tokenRepository
}
